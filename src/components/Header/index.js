import React from 'react'
import {Link} from 'gatsby'
import PropTypes from "prop-types"
import './header.css'


const Header =(props)=>{
  return(
    <header className="header">
      <div className="header-title-container">
        <h1 className="header-title">
           <Link className="header-link" to = "/" style = {{color: `white`,textDecoration: `none`, }} >
              {props.title}
          </ Link>
        </h1>
      </div>
    </header>
  )
}

export default Header